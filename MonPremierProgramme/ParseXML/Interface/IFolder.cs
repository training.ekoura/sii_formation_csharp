﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParseXML.Interface
{
    public interface IFolder
    {
        void setFolders(string[] list_folders);
        void setFiles(string[] list_files);
        List<Folder> getSubFolders();
        void exploreContent();
        int getTotalNumberOfFiles();
        int getTotalNumberOfFolders();
        long getTotalFileSize();
    }

}
