﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParseXML
{
    class Program
    {
        static void Main(string[] args)
        {
            string root_folder = @"C:\Users\gavod\source\repos";
            Folder rootFolder = new Folder(root_folder);
            rootFolder.exploreContent();

            //load from disk
            //string input = "output.xml";
            //Folder rootFolder = Persistency.loadFromDisk(input);
            //string root_folder = rootFolder.folder_path;

            Console.WriteLine("Analysed folder: {0}\n", root_folder);
            Console.WriteLine("Total number of files: {0}", rootFolder.getTotalNumberOfFiles());
            Console.WriteLine("Total number of folders: {0}", rootFolder.getTotalNumberOfFolders());
            Console.WriteLine("Total size in Megabytes: {0}", rootFolder.getTotalFileSize() / 1024 / 1024);

            Persistency.saveOnDisk(rootFolder, "output.xml");

            //rootFolder = Persistency.loadFromDisk("output.xml");
            //Persistency.saveOnDisk(rootFolder, "output2.xml");

            Console.ReadLine();

        }
    }
}
