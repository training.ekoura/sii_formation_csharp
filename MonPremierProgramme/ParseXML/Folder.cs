﻿using ParseXML.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ParseXML
{
    public class Folder : IFolder
    {
        [XmlArray("Subfolders")]
        [XmlArrayItem("Subfolder")]
        public List<Folder> folders;
        public List<File> files;

        public string folder_path;
        [XmlAttribute]
        public string folder_name;
        [XmlAttribute]
        public int nb_subfolders;
        [XmlAttribute]
        public int nb_files;

        //constructors
        public Folder()
        {
            folders = new List<Folder>();
            files = new List<File>();
        }

        public Folder(string folder_path)
        {
            this.folder_path = folder_path;
            this.folder_name = new DirectoryInfo(folder_path).Name;

            folders = new List<Folder>();
            files = new List<File>();
        }


        //transform the list of folders from a String array to a List of Folder
        public void setFolders(string[] list_folders)
        {
            Folder f_tmp;

            foreach (string f in list_folders)
            {
                f_tmp = new Folder(f);
                folders.Add(f_tmp);
            }

            nb_subfolders = folders.Count;
        }

        //transform the list of files from a String array to a List of File
        public void setFiles(string[] list_files)
        {
            //File f_tmp;

            foreach (string f in list_files)
            {
                //f_tmp = new File(f);
                //files.Add(f_tmp);
                files.Add(new File(f));
            }

            nb_files = files.Count;
        }

        public List<Folder> getSubFolders()
        {
            return folders;
        }

        //explore recursively the folder
        public void exploreContent()
        {
            string[] list_directories;
            string[] list_files;

            //list folders and fill the Folder object
            list_directories = Directory.GetDirectories(folder_path);
            setFolders(list_directories);

            //list files and fill the Folder object with files
            list_files = Directory.GetFiles(folder_path);
            setFiles(list_files);

            nb_files = list_files.Length;
            nb_subfolders = list_directories.Length;

            //recursively check subfolders
            foreach (Folder f in getSubFolders())
            {
                f.exploreContent();
            }
        }

        //get recursively the number of files in the folder and subfolders
        public int getTotalNumberOfFiles()
        {
            int nb_files = this.nb_files;

            foreach (Folder f in folders)
            {
                nb_files += f.getTotalNumberOfFiles();
            }

            return nb_files;
        }

        //get recursively the number of folders in the folder and subfolders
        public int getTotalNumberOfFolders()
        {
            int nb_folders = this.nb_subfolders;

            foreach (Folder f in folders)
            {
                nb_folders += f.getTotalNumberOfFolders();
            }

            return nb_folders;
        }

        //get recursively the total space occupied by files
        public long getTotalFileSize()
        {
            long size = 0;

            //size of the files in the current folder
            foreach (File f in files)
            {
                size += f.getFileSize();
            }

            foreach (Folder f in folders)
            {
                size += f.getTotalFileSize();
            }

            return size;
        }
    }

}
