﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ParseXML
{
    public static class Persistency
    {
        public static bool saveOnDisk(Folder folder, string filepath)
        {
            bool result = false;
            try
            {
                XmlSerializer xs = new XmlSerializer(folder.GetType());
                using (StreamWriter wr = new StreamWriter(filepath))
                {
                    xs.Serialize(wr, folder);
                }
                result = true;
            }
            catch
            {
                result = false;
            }

            return result;
        }

        public static Folder loadFromDisk(string filepath)
        {
            Folder folder = new Folder();

            try
            {
                XmlSerializer xs = new XmlSerializer(folder.GetType());
                using (StreamReader rd = new StreamReader(filepath))
                {
                    folder = xs.Deserialize(rd) as Folder;
                }
            }
            catch (Exception e)
            {
                //todo
                Console.WriteLine(e);
            }

            return folder;
        }

    }
}
