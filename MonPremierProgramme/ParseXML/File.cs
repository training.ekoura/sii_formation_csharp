﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ParseXML
{
    public class File
    {
        [XmlAttribute]
        public string filepath;
        [XmlAttribute]
        public long filesize;
        [XmlAttribute]
        public string extension;
        public string filename;

        public File(string filepath)
        {
            this.filepath = filepath;
            this.extension = Path.GetExtension(filepath);
            this.filesize = new System.IO.FileInfo(filepath).Length;
            this.filename = Path.GetFileName(filepath);
        }

        public File()
        {

        }

        public long getFileSize()
        {
            return this.filesize;
        }
    }

}
