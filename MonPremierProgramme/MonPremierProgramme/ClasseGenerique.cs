﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonPremierProgramme
{
    //static class ClasseGenerique<S> where S : struct
    static class ClasseGenerique<S>
    {
        public static void Permut(ref S t1, ref S t2)
        {
            S tmp; tmp = t1; t1 = t2; t2 = tmp;
        }
    }
}
