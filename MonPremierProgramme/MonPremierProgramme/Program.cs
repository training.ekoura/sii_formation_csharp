﻿using MonPremierProgramme.models;
using MonPremierProgramme.models.Animaux;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MonPremierProgramme
{
    class Program
    {
        #region ---------- Fonctions Persos ----------------


        static void ChangerPersonne(Personne p)
        {
            p.Nom = "Formation Csharp";
        }

        static void ChangeIntValue(in int param) {
           // param = 25;
        }
        public static void ListParams(int age, string nom , params int[] list)
        {
            for (int i = 0; i < list.Length; i++)
            {
                Console.WriteLine(list[i]);
            }
        }

        public void ClassGenerique() {
            int first = 10; // 25
            int second = 25; // 10

            string firstName = "Godwin"; // SII
            string secondName = "SII"; // Godwin

            Chien ch = new Chien();
            ch.CountYeux = 2;
            ch.Nom = "George";

            Chien ch2 = ch.Clone() as Chien;
            ch2.CountYeux = 12;

            ClasseGenerique<int>.Permut(ref first, ref second);
            ClasseGenerique<string>.Permut(ref firstName, ref secondName);
            ClasseGenerique<Chien>.Permut(ref ch, ref ch2);


            Console.WriteLine("first = {0}, second = {1}", first, second);
            Console.WriteLine("firstName = {0}, second = {1}", firstName, secondName);
            Console.WriteLine("Chien 1 = {0}, \n second = {1}", ch.ToString(), ch2.ToString());
        }

        public void DateTimeChap() {
            Console.WriteLine(DateTime.Now);
            Console.WriteLine(DateTime.UtcNow);
            Console.WriteLine(DateTime.Today);


            DateTime date = new DateTime(2020, 5, 12);
            Console.WriteLine(date.ToString());

            var sDate = "2020/12/01";
            Console.WriteLine("Conversion de la date en objet => " + DateTime.Parse(sDate));

            TimeSpan interval = DateTime.Now - date;
            Console.WriteLine("Interval TotalDays => " + interval.TotalDays.ToString());
            Console.WriteLine("Interval TotalHours => " + interval.TotalHours.ToString());
            Console.WriteLine("Interval TotalMinutes => " + interval.TotalMinutes.ToString());
        }

        public void StrinChap() {

            String s1 = "Angular";
            String s2 = "Angular";

            Console.WriteLine("s1 = {0}, s2 = {0} ", s1, s2);

            s1 = "C++";

            Console.WriteLine("s1 = {0}, s2 = {0} ", s1, s2);

            s2 = "C#";
            Console.WriteLine("s1 = {0}, s2 = {0} ", s1, s2);

            s1 = "C#";
            Console.WriteLine("s1 = {0}, s2 = {0} ", s1, s2);


            StringBuilder str = new StringBuilder();

            str.Append("Nous sommes en formations");

            var nom = "AVODAGBE";
            var prenom = "Godwin";
            var nomComplet = string.Format(" {0} {1}", nom, prenom);

            string s = String.Format("It is now {0:d} at {0:t}", DateTime.Now);
            Console.WriteLine(s);


            Console.WriteLine(DateTime.Now);
        }


        public void RegexChap() {
            Regex reg = new Regex(@"^([\w]+)@([\w]+)\.([\w]+)$");

            var adresse = "godwinavodagbe@ekoura.com";
            if (reg.IsMatch(adresse))
            {
                Console.WriteLine("Match OK");
            }
            else
                Console.WriteLine("Match KO");



            Regex regreplace = new Regex(@"Salut|Plop");

            string chaine = "Salut;Plop,Salut,Salut Nous somme en ;formation ;C#";
            string res = regreplace.Replace(chaine, "Bonjour");

            string newRes = chaine.Replace("Salut", "Bonjour")
                                .Replace("Plop", "Bonjour");

            Console.WriteLine(chaine);
            Console.WriteLine(res);
            Console.WriteLine(newRes);

            Regex regsplit = new Regex(@";|,|_");
            string[] resData = regsplit.Split(chaine);



            foreach (string item in resData)
            {
                Console.WriteLine(item);
            }

            var inputInfo = "145_IDPAYS12_125";
            string[] inputData = regsplit.Split(inputInfo);
            Console.WriteLine(inputData.Length);
            Console.WriteLine("Infortion en position 1 ==> {0}, Infortion en position 2 ==> {1}, Infortion en position 3 ==> {2}, ",
                inputData[0], inputData[1], inputData[2]);
        }

        public void DirFileChap() {
            var folderPath = @"C:\Users\gavod\source\repos\training_mvc_akka\TrainingRestAPI\TrainingRestAPI";
            Console.WriteLine("************** Fichiers dans le répertoire {0}", folderPath);
            DirectoryInfo di = new DirectoryInfo(folderPath);
            if (di.Exists)
            {
                foreach (FileInfo fi in di.GetFiles())
                {
                    Console.WriteLine(fi.FullName);
                }
            }

            var path = @"C:\Users\gavod\source\repos\training_mvc_akka\TrainingRestAPI\TrainingRestAPI";
            Console.WriteLine();
            Console.WriteLine("************** Dossiers dans le répertoire {0}", path);
            DirectoryInfo nDi = new DirectoryInfo(path);
            if (nDi.Exists)
            {
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    Console.WriteLine(dir.FullName);
                }
            }

            string filePath = @"C:\Users\gavod\source\repos\training_mvc_akka\TrainingRestAPI\TrainingRestAPI\Web.Release.config";
            if (File.Exists(filePath))
            {
                string[] content = File.ReadAllLines(filePath);

                foreach (string item in content)
                {
                    Console.WriteLine(item);
                }
            }


            Console.WriteLine("------------------------- Ecriture d'un nouveau fichier xls");
            string newPath = @"C:\Users\gavod\Desktop\test.xls";
            if (!File.Exists(newPath))
            {
                // Create a file to write to.
                string[] createText = { "Hello Nous sommes en formation", "Je m'appelle Godwin AVODAGBE", "Nous sommes le 18/06/2020" };
                File.WriteAllLines(newPath, createText);
            }

            Console.WriteLine("------------------------- Lecture d'un nouveau fichier xls");
            if (File.Exists(newPath))
            {
                string[] content = File.ReadAllLines(newPath);

                foreach (string item in content)
                {
                    Console.WriteLine(item);
                }
            }
        }

        public static void ListAndLink() {
            IList<Personne> pers = new List<Personne>();

            Adresse add = new Adresse(12, "rue de corbeil", "91356", "Essone", "Canada");
            Personne personne1 = new Eleve("SII Formation", "Toulouse", add, 12); // Polymorphisme
            Personne personne2 = new Personne("M2i Formation", "Marseille", add);
            Personne personne3 = new Personne("Feel Europe", "Montpellier", add);
            Personne personne4 = new Personne("Ekoura", "Nantes", add);
            Personne personne5 = new Personne("Edu Formation", "Lille", add);
            Personne personne6 = new Personne("Ajc Formation", "Paris", add);

            // Add item in List
            pers.Add(personne1);
            pers.Add(personne2);
            pers.Add(personne3);
            pers.Add(personne4);
            pers.Add(personne5);
            pers.Add(personne6);



            Console.WriteLine("Saisissez les 3 premiers caractères du nom de la personne que vous recherchez !!! :)");
            var selectedPersonneName = Console.ReadLine();
            Personne selectedPersonne = pers.FirstOrDefault(x => x.Nom.ToLower().StartsWith(selectedPersonneName.ToLower()));

            /*List<Personne> selectedPersons = pers.Where(p => p.Nom.ToLower().Contains(selectedPersonneName.ToLower()))
                                                    .ToList();*/


            var searchString = "SII Formation";
            var indexOfValue = searchString.ToLower().IndexOf(selectedPersonneName.ToLower()); ;

            var selectedPersons = (
                                    from p in pers
                                    where p.Nom.ToLower().IndexOf(selectedPersonneName.ToLower()) >= 0 
                                    select p
                                    )
                                  .ToList();

            /*
            foreach (Personne x in pers)
            {
                if (item.Nom.ToLower().StartsWith(selectedPersonneName.ToLower())) {
                    selectedPersonne = item;
                    break;
                }
            }*/

            /*List<Personne> _selectedPersons = new List<Personne>();
            foreach (Personne p in selectedPersons)
            {
                if (p.Nom.ToLower().Contains(selectedPersonneName.ToLower()))
                {
                    _selectedPersons.Add(p);
                }
            }*/


            if (selectedPersonne == null)
                Console.WriteLine("Aucun utilisateur trouvé");
            else
                Console.WriteLine("Utilisateur selectionné ==> " + selectedPersonne.ToString());

            Console.WriteLine();
            Console.WriteLine("Affiche les formations dont le nom contient la chaine recherchée.");
            if (selectedPersons.Count == 0)
                Console.WriteLine("Aucune personne ne correspond");
            else
            {
                var count = 1;
                foreach (Personne item in selectedPersons)
                {
                    Console.WriteLine(count.ToString() + " - " + item.ToString());
                    count++;
                }
            }
        }
        #endregion


        static void Main(string[] args)
        {

            ListAndLink();

            /*
            Dictionary<int, string> fruits = new Dictionary<int, string>();

            fruits.Add(1, "Orange");
            fruits.Add(2, "Kiwi");
            fruits.Add(3, "Banane");
            fruits.Add(4, "Raisin");

            foreach (var item in fruits)
            {
                Console.WriteLine("Id = {0} , Value = {1}", item.Key, item.Value);
            }
            */

            Console.ReadKey();
        }

        static void Training() {
            int age = 11, numero = 12;

            string nom = "Godwin";

            Console.WriteLine("Bonjour nous sommes en formation !!! ");
            Console.WriteLine("Nom : " + nom);
            Console.WriteLine("Mon age " + (age + numero).ToString());

            string famille = "Formation";

            var nomDeFamille = famille ?? "SII";

            var checkIsEqual = (nomDeFamille.Equals(famille)) ? true : false;


            age += 12; // age = age +12;
            age -= 12; // age = age - 12;
            age /= 12; // age = age / 12;
            age *= 12; // age = age * 12;

            var simonAge = ++age; // age = age + 1 ; simonAge = age;  
            var robertAge = age++; //robertAge = age; age = age + 1 ;  
        }

        static void TrainingLoop() {

            string[] noms = new string[] { "AVODAGBE", "Godwin", "Test" };
            for (int i = 0; i < noms.Length; i++)
            {
                Console.WriteLine(noms[i]);
            }
        }

        static void AsIs() {
            // creating and initializing object array 
            /*object[] obj = new object[5];
            obj[0] = new Tesla();
            obj[1] = new Tesla();
            obj[2] = "C#";
            obj[3] = 334.5;
            obj[4] = null;

            for (int j = 0; j < obj.Length; ++j)
            {

                if (obj[j] is Tesla t)
                {
                    Console.WriteLine("Objet de type Tesla");
                }

                // using as operator 
                string str = obj[j] as string;

                double? ta;

                int? tar;

                tar = null;

                ta = null;

                Console.Write("{0}:", j);


                // checking for the result 
                if (str != null)
                {
                    Console.WriteLine("'" + str + "'");
                }
                else
                {
                    Console.WriteLine("Is is not a string");
                }
            }*/
        }

        #region ------------------------------ Exos ------------------------------------------
        static void PremierExercice() {
            try
            {
                Console.Write("Veuillez renseigner le nom du produit : ");
                var nom = Console.ReadLine();

                Console.WriteLine();
                Console.Write("Veuillez renseigner le prix HT : ");
                var prixHT = double.Parse(Console.ReadLine());

                Console.WriteLine();
                Console.Write("Veuillez renseigner le nombre d'articles commandés : ");
                var qte = int.Parse(Console.ReadLine());

                Console.WriteLine();
                Console.Write("Veuillez renseigner le taux de TVA : ");
                var tva = double.Parse(Console.ReadLine());

                var t = tva/100;

                string texte = qte + " " + nom + "\nPrix total = " + (qte * (prixHT * (1 + (tva /100))));
                Console.WriteLine(prixHT * (1 + (tva /100)));

                Console.WriteLine("/*******************************************************************************************************/");
                Console.WriteLine("Vous avez commandé : " + texte);
                Console.WriteLine("/*******************************************************************************************************/");
            }
            catch (Exception)
            {

            }
            finally {
                PremierExercice();
            }
            
        }

        static void DeuxiemeExercice() {
            try
            {
                Console.Write("Veuillez renseigner votre note :  ");
                var note = double.Parse(Console.ReadLine());

                switch (note)
                {
                    case double n when (n >= 0 && n < 8):
                        Console.WriteLine("Vous avez la mention 'Recalé(e)'");
                        break;

                    case double n when (n >= 8 && n < 10):
                        Console.WriteLine("Vous avez la mention 'Rattrapage'");
                        break;

                    case double n when (n >= 10 && n < 12):
                        Console.WriteLine("Vous avez la mention 'Passable'");
                        break;

                    case double n when (n >= 12 && n < 14):
                        Console.WriteLine("Vous avez la mention 'Assez bien'");
                        break;

                    case double n when (n >= 14 && n <= 20):
                        Console.WriteLine("Vous avez la mention 'Bien'");
                        break;
                }
            }
            catch (Exception)
            {

            }
            finally {
                DeuxiemeExercice();
            }
        }

        static void TroisiemeExercice() {
            try
            {

                Console.Write("Saisissez le premier chiffre (A) :  ");
                var premier = double.Parse(Console.ReadLine());

                Console.WriteLine();
                Console.Write("Saisissez le deuxieme chiffre (B):  ");
                var second = double.Parse(Console.ReadLine());

                Console.WriteLine();
                Console.Write("Saisissez le troisime chiffre (C):  ");
                var third = double.Parse(Console.ReadLine());

                Console.WriteLine();
                if (premier > second && premier > third)
                {
                    Console.WriteLine("La note A est la plus grande");
                }
                else if (second > third && second > premier)
                {
                    Console.WriteLine("La note B est la plus grande");
                }
                else if (third > second && third > premier)
                {
                    Console.WriteLine("La note C est la plus grande ");
                }

            }
            catch (Exception)
            {
            }
            finally
            {
                TroisiemeExercice();
            }
        }

        static void QuatriemeExercice()
        {
            try
            {
                double[] data = new double[3];
                double temp;
                Console.Write("Saisissez le premier chiffre (A) :  ");
                data[0] = double.Parse(Console.ReadLine());

                Console.WriteLine();
                Console.Write("Saisissez le deuxieme chiffre (B):  ");
                data[1] = double.Parse(Console.ReadLine());

                Console.WriteLine();
                Console.Write("Saisissez le troisime chiffre (C):  ");
                data[2] = double.Parse(Console.ReadLine());

                Console.WriteLine();

                if (data[1] < data[0])
                {
                    temp = data[1];
                    data[1] = data[0];
                    data[0] = temp;
                }

                if (data[1] > data[2])
                {
                    temp = data[2];
                    data[2] = data[1];
                    data[1] = temp;
                }

                Console.WriteLine("la valeur max est" + data[2] + "la valeur mediane est" + data[1] + "la valeur min est" + data[0]);

            }
            catch (Exception)
            {
            }
            finally
            {
                QuatriemeExercice();
            }
        }


        static public int indiceMin(int[] tab, int indiceDemarrage)
        {
            int minValue = int.MaxValue;
            int indiceMinValue = -1;
            for (int i = indiceDemarrage; i < tab.Length; i++)
            {
                if (tab[i] < minValue)
                {
                    minValue = tab[i];
                    indiceMinValue = i;
                }
            }
            return indiceMinValue;
        }
        static public void echangerValeur(int[] tab, int indiceUn, int indiceDeux)
        {
            int temp;
            temp = tab[indiceUn];
            tab[indiceUn] = tab[indiceDeux];
            tab[indiceDeux] = temp;
        }
        static public void exercice4()
        {
            Console.WriteLine("Entrer 3 valeur");
            /*Scanner scan = new Scanner(System.in);
            int valeur1 = scan.nextInt();
            int valeur2 = scan.nextInt();
            int valeur3 = scan.nextInt();
            int tab[] = { valeur1, valeur2, valeur3 };
            for (int i = 0; i < tab.length; i++)
            {
                int indiceMin = indiceMin(tab, i);
                if (indiceMin != i)
                {
                    echangerValeur(tab, i, indiceMin);
                }
            }
            for (int i = 0; i < tab.length; i++)
            {
                System.out.print(tab[i] + " ");
            }*/
        }
        #endregion
    }
}
