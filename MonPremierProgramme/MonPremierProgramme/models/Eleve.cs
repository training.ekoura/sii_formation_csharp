﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonPremierProgramme.models
{
    class Eleve : Personne
    {
        public int Age { get; set; }

        public DateTime BirthDay { get; set; }

        public override string SePresente()
        {
            return "Identifiant :" + Identifiant + "; Nom :" + Nom + "; Prénom : " + Prenom + "; Adresse : " + Adresse.ShowAdresse() + "; Age : " + Age.ToString() +  ". Je suis un(e) elève";
        }

        public Eleve(string nom, string prenom, Adresse adresse, int age) : 
            base(nom, prenom, adresse)
        {
            Age = age;
        }

        public Eleve(Adresse adresse, int age) :
            base(adresse)
        {
            Age = age;
        }
    }
}
