﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonPremierProgramme.models.Animaux
{
    class Chien : Animal, ICloneable
    {
        
        public override void Crier()
        {
            Console.WriteLine("J'aboie");
        }

        public void SePresenter() {
            Console.WriteLine("Nom : " + Nom);
            //Console.WriteLine("Tete : " + Tete);
            Console.WriteLine("CountYeux : " + CountYeux);
        }


        public object Clone()
        {
            return new Chien(this);
        }


        public Chien()
        {}
        // constructeur 
        public Chien(Chien chien)
        {
            Nom = chien.Nom;
            CountYeux = chien.CountYeux;
        }

        public override string ToString()
        {
            return String.Format("Nom : {0}, CountYeux : {1}", Nom, CountYeux);
        }

    }
}
