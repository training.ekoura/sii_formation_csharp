﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonPremierProgramme.models.Animaux
{
    class Generique<T> where T : struct
    {
        public static void Permut(ref T t1, ref T t2)
        {
            T tmp; tmp = t1; t1 = t2; t2 = tmp;
        }
    }
}
