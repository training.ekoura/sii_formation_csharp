﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonPremierProgramme.models.Animaux
{
    abstract class Animal
    {
        private string Tete { get; set; }
        public int CountYeux { get; set; }
        public string Nom { get; set; }


        public abstract void Crier();
    }
}
