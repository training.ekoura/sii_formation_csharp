﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonPremierProgramme.models.Interface
{
    interface IRecruter
    {
        void Recruter();
    }
}
