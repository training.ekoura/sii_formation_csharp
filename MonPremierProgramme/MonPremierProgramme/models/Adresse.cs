﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonPremierProgramme.models
{
    class Adresse
    {
        public int NumeroRue { get; set; }
        public string Rue { get; set; }
        public string CodePostal { get; set; }
        public string Ville { get; set; }
        public string Pays { get; set; }

        public Adresse(int _num, string _rue, string _codePostal, string _ville, string _pays)
        {
            NumeroRue = _num;
            Rue = _rue;
            CodePostal = _codePostal;
            Ville = _ville;
            Pays = _pays;
        }

        public string ShowAdresse() {
            return "NumeroRue : " + NumeroRue.ToString() +
                "; Rue : " + Rue +
                "; CodePostal : " + CodePostal +
                "; Ville : " + Ville +
                "; Pays : " + Pays;

        }

    }
}
