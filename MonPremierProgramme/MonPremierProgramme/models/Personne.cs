﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MonPremierProgramme.models
{
    class Personne
    {

        private static int _Identifiant = 0;

        public int Identifiant
        {
            get { return _Identifiant; }
            set { _Identifiant = value; }
        }


        public Adresse Adresse { get; set; }

        public string Nom { get; set; }
        public string Prenom { get; set; }


        public string FullName
        {
            get { return Nom + " " + Prenom ; }
        }
         

        public string GetFullName() {

            return Nom + " " + Prenom;
        }

        public virtual string SePresente() {
            return "Identifiant :" + Identifiant + "; Nom :" + Nom + "; Prénom : " + Prenom + "; Adresse : " + Adresse.ShowAdresse() + ". Je suis une personne.";
        }

        public override string ToString()
        {
            return "Identifiant :" + Identifiant + "; Nom :" + Nom + "; Prénom : " + Prenom + "; Adresse : " + Adresse.ShowAdresse() + ". Je suis une personne.";
        }

        public override bool Equals(object obj)
        {
            return Nom.Equals(((Personne)obj).Nom);
            //return base.Equals(obj);
        }

        public Personne(string nom, string prenom, Adresse adresse)
        {
            Adresse = adresse;
            Nom = nom;
            Prenom = prenom;
            ++Identifiant;
        }


        public Personne(Adresse adresse)
        {
            Adresse = adresse;
            ++Identifiant;
        }

        public Personne()
        {

        }

    }
}
