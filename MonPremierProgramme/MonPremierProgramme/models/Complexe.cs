﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonPremierProgramme.models
{
    class Complexe
    {
        float reel;
        float imag;
        public Complexe(float r, float i)
        {
            reel = r;
            imag = i;
        }

        public Complexe Plus(Complexe z)
        {
            return new Complexe(reel + z.reel, imag + z.imag);
        }

        public static Complexe operator + (Complexe c1, Complexe c2)
        {
            return c1.Plus(c2);
        }
}
}
