﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionStock
{
    class Program
    {
        static int Rechercher(List<Article> L, int r)             //fonction qui permet de vérifier l'existence d'un article
        {
            int p = -1;
            for (int i = 0; i < L.Count; i++)
            {
                if (L[i].Numero == r)
                {
                    p = i;
                    break;
                }
            }
            return p;
        }
        static void Main(string[] args)
        {
            int choix, num, quantite, p;
            Article selectedArticle = null;
            double prix;
            string nom;
            List<Article> Stock = new List<Article>();
            do
            {
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("****************************************************************");
                Console.WriteLine("*******************************Menu*****************************");
                Console.WriteLine("****************************************************************");
                Console.WriteLine("1-Rechercher un article par numéro");
                Console.WriteLine("2-Ajouter un article");
                Console.WriteLine("3-Supprimer un article par numéro");
                Console.WriteLine("4-Modifier un article par numéro");
                Console.WriteLine("5-Rechercher un article par nom");
                Console.WriteLine("6-Rechercher un article par intervalle de prix de vente");
                Console.WriteLine("7-Afficher tous les articles");
                Console.WriteLine("8-Quitter");
                Console.WriteLine();
                Console.Write("Donner votre choix: ");
                choix = int.Parse(Console.ReadLine());
                switch (choix)
                {
                    case 1:
                        Console.Write("Donner le numéro de l'article à rechercher: ");
                        num = int.Parse(Console.ReadLine());

                        p = Rechercher(Stock, num);
                        selectedArticle = Stock.FirstOrDefault(a => a.Numero == num);
                        //if (p == -1)
                        if (selectedArticle == null)
                        {
                            Console.WriteLine("article est introuvable");
                        }
                        else
                        {
                            //Console.WriteLine(Stock[p]);
                            Console.WriteLine(selectedArticle);
                        }
                        break;

                    case 2:
                        Console.Write("Donner le numéro de l'article à ajouter: ");
                        num = int.Parse(Console.ReadLine());
                        p = Rechercher(Stock, num);
                        if (p != -1)
                        {
                            Console.WriteLine("Article existe dèjà");
                        }
                        else
                        {
                            Console.Write("Donner le nom : ");
                            nom = Console.ReadLine();
                            Console.Write("Donner le prix: ");
                            prix = double.Parse(Console.ReadLine());
                            Console.Write("Donner la quantité: ");
                            quantite = int.Parse(Console.ReadLine());
                            Stock.Add(new Article(num, nom, prix, quantite));
                            Console.WriteLine("Article Ajouté avec succès");
                        }
                        break;

                    case 3:
                        Console.Write("Donner le numéro de l'article à supprimer:");
                        num = int.Parse(Console.ReadLine());
                        p = Rechercher(Stock, num);
                        if (p == -1)
                        {
                            Console.WriteLine("Article introuvable");
                        }
                        else
                        {
                            //Stock.Remove(Stock[p]);
                            Stock.RemoveAt(p);
                            Console.WriteLine("Article supprimé avec succès");
                        }
                        break;

                    case 4:
                        Console.Write("Entrer le numéro de l'article à modifier: ");
                        num = int.Parse(Console.ReadLine());
                        p = Rechercher(Stock, num);
                        if (p == -1)
                        {
                            Console.WriteLine("Article introuvable");
                        }
                        else
                        {   //Proposer un sous menu pour choisir l'attribut à modifier
                            int c;
                            do
                            {
                                Console.WriteLine("*****Options*****");
                                Console.WriteLine("1-Modifier le nom");
                                Console.WriteLine("2-Modifier le prix");
                                Console.WriteLine("3-Modifier la quantité");
                                Console.WriteLine("4-Terminer");
                                Console.Write("Donner votre choix: ");
                                c = int.Parse(Console.ReadLine());
                                switch (c)
                                {
                                    case 1:
                                        Console.Write("Donner le nouveau nom: ");
                                        Stock[p].Nom = Console.ReadLine();
                                        Console.WriteLine("Nom modifié avec succès");
                                        break;
                                    case 2:
                                        Console.Write("Donner le prix: ");
                                        Stock[p].Prix = double.Parse(Console.ReadLine());
                                        Console.WriteLine("Prix modifié avec succès");
                                        break;
                                    case 3:
                                        Console.Write("Donner la quantité: ");
                                        Stock[p].Quantite = int.Parse(Console.ReadLine());
                                        Console.WriteLine("Quantité modifiée avec succès");
                                        break;
                                    case 4:
                                        Console.WriteLine("Modifications terminées");
                                        break;
                                    default:
                                        Console.WriteLine("Choix invalide");
                                        break;
                                }

                            }
                            while (c != 4);
                        }
                        break;

                    case 5:
                        Console.Write("Donner le nom de l'article à rechercher: ");
                        nom = Console.ReadLine();
                        int comp = 0;
                        for (int i = 0; i < Stock.Count; i++)
                        {
                            if (Stock[i].Nom.ToLower() == nom.ToLower())
                            {
                                Console.Write(Stock[i].ToString());
                                comp++;
                            }
                        }
                        if (comp == 0)
                        {
                            Console.WriteLine("Aucun résultat");
                        }
                        break;
                    case 6:
                        double min, max; int cpt = 0;
                        Console.Write("Donner le prix min :");
                        min = double.Parse(Console.ReadLine());
                        Console.Write("Donner le prix max: ");
                        max = double.Parse(Console.ReadLine());
                        if (min < 0 || max < 0 || min > max)
                        {
                            Console.WriteLine("Intervalle invalide");
                        }
                        else
                        {
                            for (int i = 0; i < Stock.Count; i++)
                            {
                                if (Stock[i].Prix >= min && Stock[i].Prix <= max)
                                {
                                    cpt++;
                                    Console.WriteLine(Stock[i]);
                                }
                            }
                            if (cpt == 0)
                            {
                                Console.WriteLine("Aucun résultat");
                            }
                        }
                        break;
                    case 7:
                        foreach (Article a in Stock)
                        {
                            Console.WriteLine(a);
                        }
                        if (Stock.Count == 0)
                        {
                            Console.WriteLine("Aucun résultat");
                        }
                        break;
                    case 8:
                        Console.WriteLine("Fin du programme");
                        break;
                    default:
                        Console.WriteLine("Choix invalide");
                        break;

                }
            } while (choix != 8);
            Console.ReadKey();
        }
    }
}
