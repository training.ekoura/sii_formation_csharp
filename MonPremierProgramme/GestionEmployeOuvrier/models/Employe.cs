﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionEmployeOuvrier.models
{
    abstract class Employe
    {
        private int matircule;
        private string nom;
        private string prenom;
        private DateTime datenaissance;

        public int Matircule
        {
            get { return matircule; }
            set { matircule = value; }
        }

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }

        public DateTime Datenaissance
        {
            get { return datenaissance; }
            set { datenaissance = value; }
        }

        public Employe(int matricule, string nom, string prenom, DateTime dn)
        {
            this.matircule = matricule;
            this.nom = nom;
            this.prenom = prenom;
            this.datenaissance = dn;
        }

        public override string ToString()
        {
            return "Matricule: " + matircule + ", Nom: " + nom + " Prénom: " + prenom + ", Date de naissance: " + datenaissance.ToShortDateString();
        }

        public abstract double GetSalaire();

    }
}
