﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TrainingXML
{
    public class Adress
    {
        public string Street { get; set; }
        public string ZipCode { get; set; }

        [XmlAttribute("City")]
        public string City { get; set; }
        
        [XmlAttribute("Country")]
        public string Country { get; set; }

        public override string ToString()
        {
            return string.Format("Street : {0}, ZipCode : {1}, City : {2}, Country : {3}", Street, ZipCode, City, Country);
        }
    }
}
