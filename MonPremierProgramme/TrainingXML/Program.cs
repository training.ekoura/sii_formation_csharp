﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TrainingXML
{
    class Program
    {

       
        static void Main(string[] args)
        {
            var persons = new List<Person>();
            var adresses = new List<Adress>();
            var add = new Adress()
            {
                City = "Paris",
                Country = "Canada",
                Street = "1-5 Paris Autralie",
                ZipCode = "12548-fr-tg"
            };

            var add2 = new Adress()
            {
                City = "Paris",
                Country = "France",
                Street = "1 Boulevard Victor",
                ZipCode = "12548-fr-tg"
            };

            adresses.Add(add);
            adresses.Add(add2);


            var p = new Person() { 
                Id = 1,
                Lastname = "AVODAGBE",
                Firstname = "Godwin",
                Adresses = adresses
            };
            
            var p1 = new Person() { 
                Id = 2,
                Lastname = "L1",
                Firstname = "F1",
                Adresses = adresses
            };
            
            var p2 = new Person() { 
                Id = 3,
                Lastname = "L2",
                Firstname = "F2",
                Adresses = adresses
            };

            persons.Add(p);
            persons.Add(p1);
            persons.Add(p2);

            Users users = new Users();
            users.Persons = persons;


            // 
            //-XmlSerializer xs = new XmlSerializer(typeof(Person));
            //XmlSerializer xs = new XmlSerializer(typeof(List<Person>));
            XmlSerializer xs = new XmlSerializer(typeof(Users));

            // 
            /*using (StreamWriter sw = new StreamWriter("personWithoutAdress.xml"))
            {
                xs.Serialize(sw, p);
            }*/

            /*using (StreamWriter sw = new StreamWriter("personWithAdress.xml"))
            {
                xs.Serialize(sw, p);
            }*/

            /*using (StreamWriter sw = new StreamWriter("ContactBook.xml"))
            {
                xs.Serialize(sw, persons);
            }*/

            /*using (StreamWriter sw = new StreamWriter("ContactBook2.xml"))
            {
                xs.Serialize(sw, persons);
            } */

            //var xmlFileName = "ContactBook3.xml";
            var xmlFileName = "ContactBook3Prime.xml";
            using (StreamWriter sw = new StreamWriter(xmlFileName))
            {
                xs.Serialize(sw, users);
            }

            /*using (StreamReader sr = new StreamReader("personWithoutAdress.xml"))
            {
                Person person = (Person)xs.Deserialize(sr);
                Console.WriteLine("Données du fichier lu ");
                Console.WriteLine(person);

                Console.ReadKey();
            }*/

            /*using (StreamReader sr = new StreamReader("personWithAdress.xml"))
            {
                Person person = (Person)xs.Deserialize(sr);
                Console.WriteLine("********************** Données lues  ********************** ");
                Console.WriteLine(person);

                Console.ReadKey();
            }*/
            
            using (StreamReader sr = new StreamReader("ContactBook.xml"))
            {
                List<Person> lPersons = (List<Person>)xs.Deserialize(sr);
                Console.WriteLine("********************** Données lues  ********************** ");

                //
                var luser = lPersons.Where(x => x.Lastname.Contains("ti")).ToList();

                foreach (Person item in lPersons)
                {
                    Console.WriteLine(item);
                    Console.WriteLine("------------------------------------------------------------");
                }

                Console.ReadKey();
            }
        }
    }
}
