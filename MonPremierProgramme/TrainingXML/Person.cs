﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TrainingXML
{
    public class Person
    {
        [XmlIgnore]
        public int Id { get; set; }
        
        [XmlElement("Nom")]
        public string  Lastname { get; set; }
        
        [XmlElement("Prenom")]
        public string Firstname { get; set; }

        [XmlArray("addresses")]
        [XmlArrayItem("Address")]
        public List<Adress> Adresses { get; set; }
        //public Adress Adress { get; set; }

        public Person(){}

        private string ShowAdresses() {
            var myAdresse = "";
            foreach (Adress item in Adresses)
            {
                myAdresse += "\n\t" + item.ToString();
            }

            return myAdresse;
        } 

        public override string ToString()
        {
            return string.Format("Id : {0}, \nLastname : {1}, \nFirstname : {2}, \nAdress : {3}", Id, Lastname, Firstname, ShowAdresses());
        }
    }
}
