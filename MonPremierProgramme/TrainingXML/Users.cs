﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TrainingXML
{
    [XmlRoot("CarnetAdresses")]
    public class Users
    {
        [XmlArray("Personnes")]
        public List<Person> Persons { get; set; }
        public string Racine { get; set; }
    }
}
