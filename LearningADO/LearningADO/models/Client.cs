﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningADO.models
{
    public class Client
    {
        public int Identifiant { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Ville { get; set; }
        public int Age { get; set; }

        public Client()
        {

        }

        public override string ToString()
        {
            return string.Format("Identifiant : {0}, Nom : {1}, Prenom : {2}, Ville : {3}, Age : {4}", Identifiant, Nom, Prenom, Ville, Age);
        }
    }
}
