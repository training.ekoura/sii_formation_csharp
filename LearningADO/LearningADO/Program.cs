﻿using LearningADO.DAL;
using LearningADO.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningADO
{
    class Program
    {
        static void Main(string[] args)
        {
            var _clientDAL = new ClientDAL();

            var clients = _clientDAL.FindAll();

            Console.WriteLine("**********************************************************");
            Console.WriteLine("******************* Liste des clients ********************");
            Console.WriteLine("**********************************************************");
            foreach (Client item in clients)
            {
                Console.WriteLine(item);
            }


            Console.WriteLine("**********************************************************");
            Console.WriteLine("******************* Infos d'un client ********************");
            Console.WriteLine("**********************************************************");


            Console.WriteLine("Veuillez saisir l'id de votre client");
            var selectedClientId = int.Parse(Console.ReadLine());

            Console.WriteLine("Ci-dessous les informations de votre client");
            Console.WriteLine(_clientDAL.FindById(selectedClientId));

            Console.WriteLine("*********************************************************************");
            Console.WriteLine("******************* Création d'un nouveau client ********************");
            Console.WriteLine("*********************************************************************");
            var nClient = new Client();
            
            Console.WriteLine("Veuillez saisir le nom votre client");
            nClient.Nom = Console.ReadLine();

            Console.WriteLine("Veuillez saisir le prénom votre client");
            nClient.Prenom = Console.ReadLine();

            Console.WriteLine("Veuillez saisir la ville de votre client");
            nClient.Ville = Console.ReadLine();

            Console.WriteLine("Veuillez saisir l'age votre client");
            nClient.Age = int.Parse(Console.ReadLine());

            _clientDAL.Create(nClient);

            Console.WriteLine(nClient);


            Console.WriteLine("*********************************************************************");
            Console.WriteLine("******************* Modification d'un client ********************");
            Console.WriteLine("*********************************************************************");

            Console.WriteLine("Veuillez saisir l'id de votre client");
            selectedClientId = int.Parse(Console.ReadLine());

            Console.WriteLine("Ci-dessous les informations de votre client");
            nClient = _clientDAL.FindById(selectedClientId);
            Console.WriteLine(nClient);

            Console.WriteLine("Veuillez saisir le nom votre client");
            nClient.Nom = Console.ReadLine();

            Console.WriteLine("Veuillez saisir le prénom votre client");
            nClient.Prenom = Console.ReadLine();

            Console.WriteLine("Veuillez saisir la ville de votre client");
            nClient.Ville = Console.ReadLine();

            Console.WriteLine("Veuillez saisir l'age votre client");
            nClient.Age = int.Parse(Console.ReadLine());

            if (_clientDAL.Update(nClient))
                Console.WriteLine("Mise à jour bien effectuée.");
            else
                Console.WriteLine("Un petit soucis");
            
            Console.WriteLine(nClient);


            Console.WriteLine("*********************************************************************");
            Console.WriteLine("******************* Suppression d'un client ********************");
            Console.WriteLine("*********************************************************************");
            Console.WriteLine("Veuillez saisir l'id du client à supprimer ");
            selectedClientId = int.Parse(Console.ReadLine());

            if (_clientDAL.DeleteById(selectedClientId))
                Console.WriteLine("Suppression bien effectuée.");
            else
                Console.WriteLine("Un petit soucis");

            Console.ReadKey();
        }
    }
}
