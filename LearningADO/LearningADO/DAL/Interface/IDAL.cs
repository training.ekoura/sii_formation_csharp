﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningADO.DAL.Interface
{
    public interface IDAL<T> where T : class
    {
        T FindById(int id);
        IList<T> FindAll();
        T Create(T obj);
        bool DeleteById(int id);
        bool Update(T obj);
    }
}
