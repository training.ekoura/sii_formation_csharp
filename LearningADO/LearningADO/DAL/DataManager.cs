﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningADO.DAL
{
    public class DataManager
    {

        private static DataManager _instance;
        private SqlConnection connection;

        private DataManager()
        {
            connection = new SqlConnection(
                                       ConfigurationManager.ConnectionStrings["ConfigDemoDatabase"].ConnectionString);
        }

        public static DataManager GetInstance() {
            if (_instance == null)
                _instance = new DataManager();

            return _instance;
        }

        public SqlConnection GetConnection() {
            return connection;
        }

    }
}
