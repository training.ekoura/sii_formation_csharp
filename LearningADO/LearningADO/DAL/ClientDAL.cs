﻿using LearningADO.DAL.Interface;
using LearningADO.models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningADO.DAL
{
    public class ClientDAL : IDAL<Client>
    {
        private SqlConnection _conn = DataManager.GetInstance().GetConnection(); 
        public Client Create(Client obj)
        {
            _conn.Open();

            using (SqlCommand _command = _conn.CreateCommand())
            {

                _command.CommandText = "insert into Client ([prenom],[nom],[ville],[age]) VALUES (@prenom, @nom, @ville, @age); " +
                                        "select @@identity";

                _command.Parameters.AddWithValue("@prenom", obj.Prenom);
                _command.Parameters.AddWithValue("@nom", obj.Nom);
                _command.Parameters.AddWithValue("@ville", obj.Ville);
                _command.Parameters.AddWithValue("@age", obj.Age);

                obj.Identifiant = int.Parse(_command.ExecuteScalar().ToString());
            }

            _conn.Close();

            return obj;
        }

        public bool DeleteById(int id)
        {
            var selectedClient = FindById(id);

            if (selectedClient != null)
            {
                _conn.Open();

                using (SqlCommand _command = _conn.CreateCommand())
                {

                    _command.CommandText = "delete from Client where identifiant = @id";

                    _command.Parameters.AddWithValue("@id", id);

                    var returnStatus = Convert.ToBoolean(_command.ExecuteNonQuery());

                    _conn.Close();

                    return returnStatus;
                }
            }
            else
                return false;
        }

        public IList<Client> FindAll()
        {
            IList<Client> clients = new List<Client>();

           
            using (SqlCommand command = _conn.CreateCommand())
            {
                _conn.Open();

                command.CommandText = "SELECT [identifiant],[prenom],[nom],[ville],[age] FROM [client]";

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read()) {
                        
                        clients.Add(
                            new Client()
                            {
                                Identifiant = reader.GetInt32(0),
                                Prenom = reader.GetString(1),
                                Nom = reader.GetString(2),
                                Ville = reader.GetString(3),
                                Age = reader.GetInt32(4)

                            }
                        );
                    }

                    reader.Close();
                }
            }

            _conn.Close();

            return clients;
        }

        public Client FindById(int id)
        {
            Client selectedClient = null;

            using (SqlCommand _command = _conn.CreateCommand())
            {
                _conn.Open();

                _command.CommandText = "SELECT [identifiant],[prenom],[nom],[ville],[age] FROM [client]" +
                    " where identifiant = @id";

                _command.Parameters.AddWithValue("@id", id);

                using (SqlDataReader _reader = _command.ExecuteReader())
                {
                    while(_reader.Read()){
                        selectedClient = new Client()
                        {
                            Identifiant = _reader.GetInt32(0),
                            Prenom = _reader.GetString(1),
                            Nom = _reader.GetString(2),
                            Ville = _reader.GetString(3),
                            Age = _reader.GetInt32(4)
                        };
                    }

                    _reader.Close();
                }
            }

            _conn.Close();
            return selectedClient;
        }

        public bool Update(Client obj)
        {
            _conn.Open();

            using (SqlCommand _command = _conn.CreateCommand())
            {

                _command.CommandText =  "update Client " +
                                        "   set [prenom] = @prenom," +
                                        "       [nom] = @nom," +
                                        "       [ville] = @ville," +
                                        "       [age] = @age" +
                                        " where identifiant = @id";

                _command.Parameters.AddWithValue("@prenom", obj.Prenom);
                _command.Parameters.AddWithValue("@nom", obj.Nom);
                _command.Parameters.AddWithValue("@ville", obj.Ville);
                _command.Parameters.AddWithValue("@age", obj.Age);
                _command.Parameters.AddWithValue("@id", obj.Identifiant);

                var returnStatus = Convert.ToBoolean(_command.ExecuteNonQuery());

                _conn.Close();

                return returnStatus;
            }
        }
    }
}
