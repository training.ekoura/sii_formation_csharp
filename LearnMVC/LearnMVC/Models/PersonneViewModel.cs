﻿using CoreData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LearnMVC.Models
{
    public class PersonneViewModel
    {
        public Personne Personne { get; set; }
        public Adresse Adresse { get; set; }
    }
}