﻿using CoreData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LearnMVC.Models
{
    public class FormationAvecAvisDto
    {
        public Formation Formation { get; set; }
        public double Note { get; set; }
    }
}