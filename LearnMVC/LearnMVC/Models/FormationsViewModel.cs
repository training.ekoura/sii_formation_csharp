﻿using CoreData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LearnMVC.Models
{
    public class FormationsViewModel
    {
        public List<Formation> Formations { get; set; }

        public FormationsViewModel()
        {
            Formations = new List<Formation>();
        }
    }
}