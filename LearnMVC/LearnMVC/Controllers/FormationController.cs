﻿using CoreData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using LearnMVC.Models;

namespace LearnMVC.Controllers
{
    [Authorize(Roles = "Admin, Commercial")]
    public class FormationController : Controller
    {
        // GET: Formation
        public ActionResult Index()
        {

            using (var db = new AvisFormationEntities())
            {
                var listFormation = db.Formation.OrderBy(x => x.Nom)
                                                .ToList();

                var listformationDto = new List<FormationAvecAvisDto>();

                foreach (var f in listFormation)
                {
                    var dto = new FormationAvecAvisDto();
                    dto.Formation = f;

                    if (f.Avis.Count == 0)
                        dto.Note = 0;
                    else
                        dto.Note = Math.Round(f.Avis.Average(a => a.Note), 2);



                    listformationDto.Add(dto);
                }

                return View(listformationDto);
            }
        }

        // GET: Formation/Details/5
        public ActionResult Details(int id)
        {
            // Un rigolo
            if (id <= 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            using (var db = new AvisFormationEntities())
            {
                var selectedFormation = db.Formation.Include(f => f.Avis)
                                                .FirstOrDefault(f => f.Id == id);

                // Un rigolo
                if (selectedFormation == null)
                    return HttpNotFound();

                return View(selectedFormation);

            }
        }

        // GET: Formation/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Formation/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Formation formation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (var db = new AvisFormationEntities())
                    {
                        db.Formation.Add(formation);
                        db.SaveChanges();
                    }

                    return RedirectToAction("Index");
                }
                else
                    return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: Formation/Edit/5
        public ActionResult Edit(int id)
        {
            // Un rigolo
            if (id <= 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            using (var db = new AvisFormationEntities())
            {
                var selectedFormation = db.Formation.Include(f => f.Avis)
                                                .FirstOrDefault(f => f.Id == id);

                // Un rigolo
                if (selectedFormation == null)
                    return HttpNotFound();

                return View(selectedFormation);
            }
        }

        // POST: Formation/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Formation formation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (var db = new AvisFormationEntities())
                    {
                        db.Entry(formation).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    return RedirectToAction("Index");
                }
                else
                    return View(formation);
            }
            catch
            {
                return View();
            }
        }

        // GET: Formation/Delete/5
        public ActionResult Delete(int id)
        {

            try
            {
                using (var db = new AvisFormationEntities())
                {

                    if (id <= 0)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                    var selectedFormation = db.Formation.Find(id);

                    if (selectedFormation == null)
                        return HttpNotFound();
                    else
                    {
                        db.Formation.Remove(selectedFormation);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }
            catch
            {
                return View();
            }


            // Un rigolo
            /*if (id <= 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            using (var db = new AvisFormationEntities())
            {
                var selectedFormation = db.Formation.Include(f => f.Avis)
                                                .FirstOrDefault(f => f.Id == id);

                // Un rigolo
                if (selectedFormation == null)
                    return HttpNotFound();

                return View(selectedFormation);
            }*/

        }

        // POST: Formation/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, string nomSeo)
        {
            try
            {
                using (var db = new AvisFormationEntities())
                {
                    var selectedFormation = db.Formation.Find(id);

                    if (selectedFormation == null)
                        return HttpNotFound();
                    else {
                        db.Formation.Remove(selectedFormation);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }
            catch
            {
                return View();
            }
        }
    }
}
