﻿using CoreData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace LearnMVC.Controllers
{
    public class AvisController : Controller
    {
        // GET: Avis
        public ActionResult Index()
        {
            using (var db = new AvisFormationEntities())
            {
                return View("Accueil", db.Avis.Include(a => a.Formation).ToList());
            }
        }

        // GET: Avis/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Avis/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Avis/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Avis/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Avis/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Avis/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Avis/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
