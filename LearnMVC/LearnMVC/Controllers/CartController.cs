﻿using CoreData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LearnMVC.Controllers
{
    public class CartController : Controller
    {
        // GET: Cart
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddToCart(int formationId) {

            using (var db = new AvisFormationEntities())
            {
                var selectedFormation = db.Formation.Find(formationId);

                var myCartTraining = ((List<Formation>)Session["formations"]);

                var checkifExist = myCartTraining.FirstOrDefault(f => f.Id == formationId);

                // Ajout
                if (checkifExist == null)
                {
                    myCartTraining.Add(selectedFormation);
                }
                else {
                    myCartTraining.RemoveAll(f => f.Id == formationId);
                }

                return RedirectToAction("Index", "Formation");
            }
        }

        public ActionResult DeleteById(int formationId)
        {

            using (var db = new AvisFormationEntities())
            {
                var selectedFormation = db.Formation.Find(formationId);

                var myCartTraining = ((List<Formation>)Session["formations"]);

                var checkifExist = myCartTraining.FirstOrDefault(f => f.Id == formationId);

                // Ajout
                if (checkifExist != null)
                {
                    myCartTraining.RemoveAll(f => f.Id == formationId);
                }

                return RedirectToAction("Index");
            }

        }

        public ActionResult ClearCart()
        {
            ((List<Formation>)Session["formations"]).Clear();
            return RedirectToAction("Index");
        }

    }
}