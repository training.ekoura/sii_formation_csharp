﻿$(document).ready(function () {

    // Si tu es connecté alors tu as un token
    if (sessionStorage.getItem('token') != null) {

        // Alors je récupère la liste des formations.
        $.ajax({
            type: 'get',
            url: '/cours',
            headers: {
                Authorization: "Bearer " + sessionStorage.getItem('token')
            }
        }).done(function (data) {
            
            for (var i = 0; i < data.length; i++) {
                var row =   "<tr><th scope = 'row' >" + data[i].id + "</th>"+
                            "<td>" + data[i].nom  + "</td>" +
                            "<td>" + data[i].description + "</td></tr >"

                $('#listFormationTable').append(row);
                $('#ListFormationDiv').css("display", "block");

            }
        }).fail(error => console.log(error));
    }
});

function LoginUser() {

    $.ajax({
        type: 'POST',
        url: '/Token',
        data: {
            grant_type: 'password',
            username: $("#username").val(),
            password: $("#password").val()
        }
    }).done(function (data) {

        // Cache the access token in session storage.
        sessionStorage.setItem("token", data.access_token);

        // Je redirige l'utilisateur vers la page accueil.
        window.location.href = "index";

    }).fail(error => console.log(error));
}

function Register() {

    var dataToSend = {
        email: $('#email').val(),
        password: $("#password").val(),
        confirmpassword: $("#confirmPassword").val()
    };

    console.log("Data à envoyer", dataToSend);

    $.ajax({
        type: 'POST',
        url: '/api/account/register',
        data: dataToSend
    }).done(function (data) {

        window.location.href = "login";

    }).fail(error => console.log(error));
}