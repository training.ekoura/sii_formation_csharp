﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing;
using CoreData;

namespace LearnRest.Controllers
{
    /*
    La classe WebApiConfig peut exiger d'autres modifications pour ajouter un itinéraire à ce contrôleur. Fusionnez ces instructions dans la méthode Register de la classe WebApiConfig, le cas échéant. Les URL OData sont sensibles à la casse.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using CoreData;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Formation>("Formations");
    builder.EntitySet<Avis>("Avis"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class FormationsController : ODataController
    {
        private AvisFormationEntities db = new AvisFormationEntities();

        // GET: odata/Formations
        [EnableQuery]
        [Queryable(AllowedOrderByProperties = "Id")] // comma-separated list of properties
        public IQueryable<Formation> GetFormations()
        {
            return db.Formation;
        }

        // GET: odata/Formations(5)
        [EnableQuery]
        public SingleResult<Formation> GetFormation([FromODataUri] int key)
        {
            return SingleResult.Create(db.Formation.Where(formation => formation.Id == key));
        }

        // PUT: odata/Formations(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Formation> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Formation formation = db.Formation.Find(key);
            if (formation == null)
            {
                return NotFound();
            }

            patch.Put(formation);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FormationExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(formation);
        }

        // POST: odata/Formations
        public IHttpActionResult Post(Formation formation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Formation.Add(formation);
            db.SaveChanges();

            return Created(formation);
        }

        // PATCH: odata/Formations(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Formation> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Formation formation = db.Formation.Find(key);
            if (formation == null)
            {
                return NotFound();
            }

            patch.Patch(formation);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FormationExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(formation);
        }

        // DELETE: odata/Formations(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Formation formation = db.Formation.Find(key);
            if (formation == null)
            {
                return NotFound();
            }

            db.Formation.Remove(formation);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Formations(5)/Avis
        [EnableQuery]
        public IQueryable<Avis> GetAvis([FromODataUri] int key)
        {
            return db.Formation.Where(m => m.Id == key).SelectMany(m => m.Avis);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FormationExists(int key)
        {
            return db.Formation.Count(e => e.Id == key) > 0;
        }
    }
}
