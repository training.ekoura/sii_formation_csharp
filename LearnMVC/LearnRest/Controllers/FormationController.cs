﻿using CoreData;
using LearnRest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace LearnRest.Controllers
{
    [System.Web.Http.Authorize]
    [System.Web.Http.RoutePrefix("cours")]
    public class FormationController : ApiController
    {
        private AvisFormationEntities db = new AvisFormationEntities();

        // Typed lambda expression for Select() method. 
        private static readonly Expression<Func<Formation, FormationDto>> AsFormationDto =
            f => new FormationDto
            {
                Id = f.Id,
                Description = f.Description,
                Nom = f.Nom
            };


        // GET: api/Formation
        [System.Web.Http.Route("")]
        public IQueryable<FormationDto> Get()
        {
           //return db.Formation.Select(AsFormationDto);
           return db.Formation.Select(f => new FormationDto() {
                Id = f.Id,
                Description = f.Description,
                Nom = f.Nom
           });
        }

        // GET: api/Formation/5
        [System.Web.Http.Route("{id}")]
        public IHttpActionResult Get(int id)
        {
            if (id <= 0)
                return BadRequest();


            var formation = db.Formation.Find(id);
            if (formation == null)
                return NotFound();

            var selectedFormation = new FormationDto() {
                Id = formation.Id,
                Description = formation.Description,
                Nom = formation.Nom
            };

            return Ok(selectedFormation);
        }

        [System.Web.Http.Route("~/cours/details/{nomDeFormation}")]
        public IHttpActionResult Get(string nomDeFormation)
        {
            if (string.IsNullOrEmpty(nomDeFormation))
                return BadRequest();


            var formation = db.Formation.FirstOrDefault(f => f.Nom.ToLower().Contains(nomDeFormation.ToLower()));
            if (formation == null)
                return NotFound();

            var selectedFormation = new FormationDto()
            {
                Id = formation.Id,
                Description = formation.Description,
                Nom = formation.Nom
            };

            return Ok(selectedFormation);
        }

        // avis ==> formation 1
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("~/cours/avis/{idFormation:int:min(1)}")]
        public IHttpActionResult FormationAvis(int idFormation)
        {
           
            var formation = db.Formation.FirstOrDefault(f => f.Id == idFormation);
            if (formation == null)
                return NotFound();

            return Ok(formation.Avis);
        }


        // POST: api/Formation
        [System.Web.Http.Route("")]
        public IHttpActionResult Post([FromBody]Formation formation)
        {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            db.Formation.Add(formation);
            db.SaveChanges();

            var selectedFormation = new FormationDto()
            {
                Id = formation.Id,
                Description = formation.Description,
                Nom = formation.Nom
            };
            return CreatedAtRoute("Cours", new { id = formation.Id }, selectedFormation);
        }

        // PUT: api/Formation/5
        [System.Web.Http.Route("{id:int}")]
        public IHttpActionResult Put(int id, [FromBody]Formation formation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != formation.Id)
                return BadRequest();


            db.Entry<Formation>(formation).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);

        }

        [System.Web.Http.Route("{id}")]
        // DELETE: api/Formation/5
        public IHttpActionResult Delete(int id)
        {
            Formation formation = db.Formation.Find(id);
            if (formation == null)
            {
                return NotFound();
            }

            db.Formation.Remove(formation);
            db.SaveChanges();


            var selectedFormation = new FormationDto()
            {
                Id = formation.Id,
                Description = formation.Description,
                Nom = formation.Nom
            };

            return Ok(selectedFormation);
        }
    }
}
