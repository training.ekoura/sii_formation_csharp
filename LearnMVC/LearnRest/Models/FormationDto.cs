﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LearnRest.Models
{
    public class FormationDto
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
    }
}