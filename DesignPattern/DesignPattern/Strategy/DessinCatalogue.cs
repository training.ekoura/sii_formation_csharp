using System.Collections.Generic;

namespace DesignPattern.Strategy
{
  public interface DessinCatalogue
  {
    void dessine(IList<VueVehicule> contenu);
  }
}