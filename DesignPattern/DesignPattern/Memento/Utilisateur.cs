
namespace DesignPattern.Memento
{
  public class Utilisateur
  {

    public void LoadMemento(){
      Memento memento;
      OptionVehicule option1 = new OptionVehicule("Sites en cuir");
      OptionVehicule option2 = new OptionVehicule("Accoudoirs");
      OptionVehicule option3 = new OptionVehicule("Sites sportifs");
      option1.ajouteOptionIncompatible(option3);
      option2.ajouteOptionIncompatible(option3);
      ChariotOption chariotOptions = new ChariotOption();
      chariotOptions.ajouteOption(option1);
      chariotOptions.ajouteOption(option2);
      chariotOptions.affiche();
      memento = chariotOptions.ajouteOption(option3);
      chariotOptions.affiche();
      chariotOptions.annule(memento);
      chariotOptions.affiche();
    }
  }
}