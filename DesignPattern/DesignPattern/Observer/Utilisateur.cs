using System;

namespace DesignPattern.Observer
{
  public class Utilisateur
  {
    public void LoadObserver(){
      Vehicule vehicule = new Vehicule();
      vehicule.description = "Vehicule bon march�";
      vehicule.prix = 5000.0;
      VueVehicule vueVehicule = new VueVehicule(vehicule);
      vueVehicule.redessine();
      vehicule.prix = 4500.0;
      VueVehicule vueVehicule2 = new VueVehicule(vehicule);
      vehicule.prix = 5500.0;
    }
  }
}