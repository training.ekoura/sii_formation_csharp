namespace DesignPattern.Observer
{
  public interface Observateur
  {
    void actualise();
  }
}
