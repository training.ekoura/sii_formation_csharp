using System.Collections.Generic;
namespace DesignPattern.Prototype
{
  public abstract class Liasse
  {
    public IList<Document> documents { 
            get; 
            protected set; }
  }
}
