namespace DesignPattern.TemplateMethod
{
  public class Utilisateur
  {
    public void LoadTemplateMethod(){
      Commande commandeFrance = new CommandeFrance();
      commandeFrance.setMontantHt(10000);
      commandeFrance.calculeMontantTtc();
      commandeFrance.affiche();
      
      
      Commande commandeLuxembourg = new CommandeLuxembourg();
      commandeLuxembourg.setMontantHt(10000);
      commandeLuxembourg.calculeMontantTtc();
      commandeLuxembourg.affiche();
    }
  }
}