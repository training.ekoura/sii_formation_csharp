namespace DesignPattern.Singleton
{
  public class TestVendeur
  {
    public static void affiche()
    {
      Vendeur leVendeur = Vendeur.Instance();
      leVendeur.affiche();
    }

    public void LoadSingleton (){
      // initialisation du vendeur du syst�me
      Vendeur leVendeur = Vendeur.Instance();
      leVendeur.nom = "Vendeur Auto";
      leVendeur.adresse = "Paris";
      leVendeur.email = "vendeur@vendeur.com";
      // affichage du vendeur du syst�me
      affiche();

      Vendeur leVendeur2 = Vendeur.Instance();
      leVendeur2.nom = "Vendeur Auto Godwin";
      leVendeur2.adresse = "Lyon";
      leVendeur2.email = "vendeur@vendeurgodwin.com";
      affiche();
      
    }
  }
}