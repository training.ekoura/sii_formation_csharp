using System;

namespace DesignPattern.Bridge
{
  public interface IFormulaireImpl
  {
    void dessineTexte(string texte);
    string gereZoneSaisie();
  }
}