using System;


namespace DesignPattern.Bridge
{
  public class FormAppletImpl : IFormulaireImpl
  {

    public void dessineTexte(string texte)
    {
      Console.WriteLine("Applet : " + texte);
    }

    public string gereZoneSaisie()
    {
      return Console.ReadLine();
    }
  }
}