﻿using System;
using DesignPattern.Adapter;
using DesignPattern.FactoryMethod;
using DesignPattern.Prototype;
using DesignPattern.Singleton;
using DesignPattern.Facade;
using DesignPattern.AbstractFactory.Interface;
using DesignPattern.AbstractFactory;

namespace DesignPattern
{
    class MainClass
    {
        public static int nbAutos = 3;
        public static int nbScooters = 2;

        public static void Main(string[] args)
        {
            // 1- AbstractFactory
            /*AbstractFactory.Catalogue _catalogue = new AbstractFactory.Catalogue();
            _catalogue.LoadCatalogue();*/

            // 2- Builder
            //Builder.ClientVehicule _clientVehicule = new Builder.ClientVehicule();
            //_clientVehicule.LoadBuilder();

            // 3- FactoryMethod
            //FactoryMethod.Utilisateur _utilisateur = new FactoryMethod.Utilisateur();
            //_utilisateur.LoadFactoryMethod();

            // 4- Prototype
            //Prototype.Utilisateur _utilisateurProto = new Prototype.Utilisateur();
            //_utilisateurProto.LoadPrototype();

            // 5- Singleton
            //TestVendeur _singleton = new TestVendeur();
            //_singleton.LoadSingleton();

            // 6- Adapter
            //ServeurWeb _adapter = new ServeurWeb();
            //_adapter.LoadAdapter();

            // 7- Bridge
            Bridge.Utilisateur _bridge = new Bridge.Utilisateur();
            _bridge.LoadBridge();

            // 8- Composite
            //Composite.Utilisateur _composite = new Composite.Utilisateur();
            //_composite.LoadComposite();

            // 9- Decorator
            //VueCatalogue _decorator = new VueCatalogue();
            //_decorator.LoadDecorator();

            // 10- Facade
            //UtilisateurWebService _facade = new UtilisateurWebService();
            //_facade.LoadFacade();

            // 11- Flyweight
            //Flyweight.Client _flyweight = new Flyweight.Client();
            //_flyweight.LoadFlyweight();

            // 12- Proxy
            //Proxy.VueVehicule _vuevehicule = new Proxy.VueVehicule();
            //_vuevehicule.LoadProxy();

            // 13- ChainOfResponsibility
            //ChainOfResponsibility.Utilisateur _ChainOfResponsibility = new ChainOfResponsibility.Utilisateur();
            //_ChainOfResponsibility.LoadChainOfResponsibility();

            // 14- namespace DesignPattern.Command
            //Command.Utilisateur _command = new Command.Utilisateur();
            //_command.LoadCommand();

            // 15- namespace Interpretor
            //Interpreter.Utilisateur _interpretor = new Interpreter.Utilisateur();
            //_interpretor.LoadInterpreter();

            // 16- Iterator 
            //Iterator.Utilisateur _iterator = new Iterator.Utilisateur();
            //_iterator.LoadIterator();

            // 17- Mediator 
            //Mediator.Utilisateur _mediator = new Mediator.Utilisateur();
            //_mediator.LoadMediator();

            // 18- Memento 
            //Memento.Utilisateur _memento = new Memento.Utilisateur();
            //_memento.LoadMemento();

            // 19- Observer 
            //Observer.Utilisateur _observer = new Observer.Utilisateur();
            //_observer.LoadObserver();

            // 20- State 
            //State.Utilisateur _state = new State.Utilisateur();
            //_state.LoadState();

            // 21- Strategy
            //Strategy.Utilisateur _strategy = new Strategy.Utilisateur();
            //_strategy.LaodStrategy();

            // 22- TemplateMethod
            //TemplateMethod.Utilisateur _templateMethod = new TemplateMethod.Utilisateur();
            //_templateMethod.LoadTemplateMethod();

            // 23- Visitor
            //Visitor.Utilisateur _visitor = new Visitor.Utilisateur();
            //_visitor.LoadVisitor();

            Console.ReadKey();

        }
  }
}

