using System;


namespace DesignPattern.Adapter
{
  public interface IDocument
  {
    string contenu { set; }
    void dessine();
    void imprime();
  }
}