using System;

namespace DesignPattern.Adapter
{
  public class DocumentPdf : IDocument
  {
    protected ComposantPdf outilPdf = new ComposantPdf();

    public string contenu
    {
      set
      {
        outilPdf.pdfFixeContenu(value);
      }
    }

    public void dessine()
    {
      outilPdf.pdfPrepareAffichage();
      outilPdf.pdfRafraichit();
      outilPdf.pdfTermineAffichage();
    }

    public void imprime()
    {
      outilPdf.pdfEnvoieImprimante();
    }
  }
}