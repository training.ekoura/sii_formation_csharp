using System;

namespace DesignPattern.ChainOfResponsibility
{
  public class Modele : ObjetBase
  {
    protected string laDescription;
    protected string nom;

    public Modele(string nom, string _description)
    {
      this.laDescription = _description;
      this.nom = nom;
    }

    protected override string description
    {
      get
      {
        if (laDescription != null)
          return "Mod��le " + nom + " : " + laDescription;
        else
          return null;
      }
    }
  }
}