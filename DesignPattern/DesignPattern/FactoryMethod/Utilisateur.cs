using System;

namespace DesignPattern.FactoryMethod
{
  public class Utilisateur
  {
    public void LoadFactoryMethod()
    {
      Client client;
      client = new ClientComptant();
      client.nouvelleCommande(2000.0);
      client.nouvelleCommande(10000.0);
      client = new ClientCredit();
      client.nouvelleCommande(2000.0);
      client.nouvelleCommande(10000.0);
    }
  }
}