using System;
using System.Collections.Generic;

namespace DesignPattern.FactoryMethod
{
  public abstract class Client
  {
    protected IList<Commande> commandes = new List<Commande>();

    protected abstract Commande creeCommande(double montant);

    public void nouvelleCommande(double montant)
    {
      Commande commande = this.creeCommande(montant);
            if (commande.valide())
            {
                commande.paye();
                commandes.Add(commande);
            }
            else
                Console.WriteLine("Commande impossible.");
    }
  }
}