﻿using System;
using DesignPattern.AbstractFactory.Interface;

namespace DesignPattern.AbstractFactory
{
  public class FabriqueVehiculeEssence : IFabriqueVehicule
  {
    public Automobile creeAutomobile(string modele, string
      couleur, int puissance, double espace)
    {
      return new AutomobileEssence(modele, couleur, puissance, espace);
    }

    public Scooter creeScooter(string modele, string couleur, int puissance)
    {
      return new ScooterEssence(modele, couleur, puissance);
    }
  }
}
