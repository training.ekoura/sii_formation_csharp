﻿using System;
using DesignPattern.AbstractFactory.Interface;

namespace DesignPattern.AbstractFactory
{
  public class FabriqueVehiculeElectricite : IFabriqueVehicule
  {
    public Automobile creeAutomobile(string modele, string couleur, int puissance, double espace)
    {
      return new AutomobileElectricite(modele, couleur, puissance, espace);
    }

    public Scooter creeScooter(string modele, string couleur, int puissance)
    {
      return new ScooterElectricite(modele, couleur, puissance);
    }
  }
}
