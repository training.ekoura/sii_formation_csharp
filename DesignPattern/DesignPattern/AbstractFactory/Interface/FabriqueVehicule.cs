﻿using System;
namespace DesignPattern.AbstractFactory.Interface
{
  public interface IFabriqueVehicule
  {
    Automobile creeAutomobile(string modele, string couleur, int puissance, double espace);

    Scooter creeScooter(string modele, string couleur, int puissance);

  }
}
