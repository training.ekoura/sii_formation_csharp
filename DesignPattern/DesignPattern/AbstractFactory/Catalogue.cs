﻿using System;
using DesignPattern.AbstractFactory;
using DesignPattern.AbstractFactory.Interface;

namespace DesignPattern.AbstractFactory
{
  public class Catalogue
  {
        public static int nbAutos = 3;
        public static int nbScooters = 2;

        public Catalogue()
        {
        }

        public void LoadCatalogue(){

            IFabriqueVehicule fabrique;
            Automobile[] autos = new Automobile[nbAutos];
            Scooter[] scooters = new Scooter[nbScooters];

            Console.WriteLine("Voulez-vous utiliser " +
              "des vehicules electriques (1) ou  essence (2) :");
            string choix = Console.ReadLine();


            if (choix == "1")
            {
                fabrique = new FabriqueVehiculeElectricite();
            }
            else
            {
                fabrique = new FabriqueVehiculeEssence();
            }

            // Bloc pour la création des autos et scooters
            for (int index = 0; index < nbAutos; index++)
                autos[index] = fabrique.creeAutomobile("standard",
                  "jaune", 6 + index, 3.2);


            for (int index = 0; index < nbScooters; index++)
                scooters[index] = fabrique.creeScooter("classic",
                  "rouge", 2 + index);


            // Bloc pour l'affichage 
            foreach (Automobile auto in autos)
                auto.afficheCaracteristiques();

            foreach (Scooter scooter in scooters)
                scooter.afficheCaracteristiques();

            

        }
  }
}
