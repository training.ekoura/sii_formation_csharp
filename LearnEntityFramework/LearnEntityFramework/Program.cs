﻿using LearnEntityFramework.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnEntityFramework
{
    class Program
    {
        static void Main(string[] args)
        {
           
            using (var db = new DemoContext())
            {
                Console.WriteLine("****************************************************");
                Console.WriteLine("***************** FindAll ***************");
                Console.WriteLine("****************************************************");
                // FindAll()
                var users = db.Personne.Include(p => p.Adresse)
                                       .ToList();

                foreach (Personne item in users)
                {
                    Console.WriteLine(item);
                }


                Console.WriteLine("****************************************************");
                Console.WriteLine("***************** FindById ***************");
                Console.WriteLine("****************************************************");
                Console.WriteLine();
                Console.WriteLine("Veuillez saisir l'id de l'utilisateur à chercher ");
                var selectedUserId = int.Parse(Console.ReadLine());
                var selectedUser = db.Personne
                                        .Include( p => p.Adresse )
                                        .FirstOrDefault( x => x.Id == selectedUserId);

                Console.WriteLine(selectedUser);

                Console.WriteLine("****************************************************");
                Console.WriteLine("***************** Create ***************");
                Console.WriteLine("****************************************************");
                var add = new Adresse()
                {
                    NumeroRue = 12,
                    CodePostal = "12546",
                    Pays = "France",
                    Rue = "Charle renard",
                    Ville = "Paris"
                }; 
                var nUser = new Personne()
                {
                    Nom = "AV",
                    Prenom = "Godwin",
                    Email = "a@t.fr",
                    Age = 12,
                    Password  ="tettetetetet",
                    Datenaissance = DateTime.Now,
                    Adresse = add

                };
                // Insertion d'une nouvelle ligne dans mon entité. (Niveau App)
                db.Personne.Add(nUser);
                db.SaveChanges(); // Je propage la création au niveau de la base de données

                // Recupère les users qui sont dans ma base de données et je les affiche. 
                users = db.Personne.ToList();

                Console.WriteLine("****************************************************");
                Console.WriteLine("***************** insert / affichage ***************");
                Console.WriteLine("****************************************************");

                foreach (Personne item in users)
                {
                    Console.WriteLine(item);
                }


                Console.WriteLine("****************************************************");
                Console.WriteLine("***************** Delete / affichage ***************");
                Console.WriteLine("****************************************************");
                Console.WriteLine();
                Console.WriteLine("Veuillez saisir l'id de l'utilisateur à supprimer ");
                selectedUserId = int.Parse(Console.ReadLine());
                selectedUser = db.Personne
                                        .Include(p => p.Adresse)
                                        .FirstOrDefault(x => x.Id == selectedUserId);
                if (selectedUser == null)
                    Console.WriteLine("Not found");
                else
                {
                    db.Personne.Remove(selectedUser);
                    db.SaveChanges();
                    Console.WriteLine("Utilisateur n° {0} supprimé dans la base.", selectedUserId);
                }

                foreach (Personne item in db.Personne.ToList())
                {
                    Console.WriteLine(item);
                }

                Console.WriteLine("****************************************************");
                Console.WriteLine("***************** Update / affichage ***************");
                Console.WriteLine("****************************************************");
                Console.WriteLine();
                Console.WriteLine("Veuillez saisir l'id de l'utilisateur à modifier ");
                selectedUserId = int.Parse(Console.ReadLine());
                selectedUser = db.Personne
                                        .Include(p => p.Adresse)
                                        .FirstOrDefault(x => x.Id == selectedUserId);
                if (selectedUser == null)
                    Console.WriteLine("Not found");
                else
                {
                    selectedUser.Nom = "Mickel";
                    selectedUser.Prenom = "Jackson";

                    db.Entry(selectedUser).State = EntityState.Modified;
                    //db.SaveChanges();
                }

                foreach (Personne item in db.Personne.ToList())
                {
                    Console.WriteLine(item);
                }
            }

            Console.ReadKey();
        }
    }
}
