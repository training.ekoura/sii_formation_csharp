﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnEntityFramework.Entities
{
    [Table("User")]
    class Personne
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("Identifiant")]
        public int Id { get; set; }

        [StringLength(100)]
        public string Nom { get; set; }
        
        [StringLength(250)]
        public string Prenom { get; set; }

        public virtual Adresse Adresse { get; set; }

        [DataType(DataType.Date)]
        public DateTime Datenaissance { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }


        [EmailAddress]
        [StringLength(100)]
        public string Email { get; set; }

        public int Age { get; set; }

        public virtual ICollection<Societe> Societes { get; set; }

        public Personne()
        {
            Societes = new HashSet<Societe>();
        }

        public override string ToString()
        {
            return string.Format("Id : {0}, Nom : {1}, Prenom : {2}, Datenaissance : {3}, Email : {4} , \n\t\t Adresse : {5} ", Id, Nom, Prenom, Datenaissance, Email, Adresse);
        }
    }
}
