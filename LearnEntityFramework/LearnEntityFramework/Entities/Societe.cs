﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnEntityFramework.Entities
{
    [Table("Societe")]
    class Societe
    {
        public int Id { get; set; }
        public string Libelle { get; set; }
        public virtual ICollection<Personne> Employes { get; set; }
        public Societe()
        {
            Employes = new HashSet<Personne>();
        }
    }
}
