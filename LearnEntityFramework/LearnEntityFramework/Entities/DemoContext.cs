﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnEntityFramework.Entities
{
    class DemoContext : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
        public DbSet<Personne> Personne { get; set; }
        public DbSet<Adresse> Adresse { get; set; }
        public DbSet<Societe> Societes { get; set; }
    }
}
