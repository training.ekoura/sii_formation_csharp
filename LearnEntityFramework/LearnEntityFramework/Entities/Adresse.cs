﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnEntityFramework.Entities
{
    [Table("Adresse")]
    class Adresse
    {
        public int Id { get; set; }
        public int NumeroRue { get; set; }
        public string Rue { get; set; }
        public string CodePostal { get; set; }
        public string Ville { get; set; }
        public string Pays { get; set; }

        public override string ToString()
        {
            return string.Format("Id : {0}, NumeroRue : {1}, Rue : {2}, Datenaissance : {3}, Email : {4} ", Id, NumeroRue, Rue, CodePostal, Ville);
        }

    }
}
