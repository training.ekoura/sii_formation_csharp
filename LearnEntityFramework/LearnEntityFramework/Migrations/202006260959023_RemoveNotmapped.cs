﻿namespace LearnEntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveNotmapped : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "Adresse", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.User", "Adresse");
        }
    }
}
