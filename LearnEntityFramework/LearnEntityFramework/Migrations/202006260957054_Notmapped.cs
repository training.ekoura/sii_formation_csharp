﻿namespace LearnEntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Notmapped : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "Datenaissance", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.User", "Datenaissance");
        }
    }
}
