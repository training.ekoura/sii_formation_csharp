﻿namespace LearnEntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class List_Societe : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Societe",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Libelle = c.String(),
                        Personne_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.Personne_Id)
                .Index(t => t.Personne_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Societe", "Personne_Id", "dbo.User");
            DropIndex("dbo.Societe", new[] { "Personne_Id" });
            DropTable("dbo.Societe");
        }
    }
}
