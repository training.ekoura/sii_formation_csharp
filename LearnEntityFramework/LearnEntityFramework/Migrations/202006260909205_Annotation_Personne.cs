﻿namespace LearnEntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Annotation_Personne : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Personnes", newName: "User");
            RenameColumn(table: "dbo.User", name: "Id", newName: "Identifiant");
            AlterColumn("dbo.User", "Nom", c => c.String(maxLength: 100));
            AlterColumn("dbo.User", "Prenom", c => c.String(maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.User", "Prenom", c => c.String());
            AlterColumn("dbo.User", "Nom", c => c.String());
            RenameColumn(table: "dbo.User", name: "Identifiant", newName: "Id");
            RenameTable(name: "dbo.User", newName: "Personnes");
        }
    }
}
