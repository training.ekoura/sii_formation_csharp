﻿// <auto-generated />
namespace LearnEntityFramework.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class employeesToSocite : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(employeesToSocite));
        
        string IMigrationMetadata.Id
        {
            get { return "202006261314054_employeesToSocite"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
