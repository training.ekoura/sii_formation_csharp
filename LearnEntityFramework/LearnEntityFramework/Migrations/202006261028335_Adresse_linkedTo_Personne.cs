﻿namespace LearnEntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Adresse_linkedTo_Personne : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Adresses", newName: "Adresse");
            AddColumn("dbo.User", "Adresse_Id", c => c.Int());
            CreateIndex("dbo.User", "Adresse_Id");
            AddForeignKey("dbo.User", "Adresse_Id", "dbo.Adresse", "Id");
            DropColumn("dbo.User", "Adresse");
        }
        
        public override void Down()
        {
            AddColumn("dbo.User", "Adresse", c => c.String());
            DropForeignKey("dbo.User", "Adresse_Id", "dbo.Adresse");
            DropIndex("dbo.User", new[] { "Adresse_Id" });
            DropColumn("dbo.User", "Adresse_Id");
            RenameTable(name: "dbo.Adresse", newName: "Adresses");
        }
    }
}
