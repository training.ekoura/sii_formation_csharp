﻿namespace LearnEntityFramework.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<LearnEntityFramework.Entities.DemoContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(LearnEntityFramework.Entities.DemoContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            /*context.Personne.AddOrUpdate(new Entities.Personne()
            {
                Age = 21,
                Id = 10,
                Nom= "Gav",
                Prenom ="Av",
                Datenaissance = DateTime.Now,
                Email = "gat@t.fr",
            });*/

        }
    }
}
