﻿namespace LearnEntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class employeesToSocite : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Societe", "Personne_Id", "dbo.User");
            DropIndex("dbo.Societe", new[] { "Personne_Id" });
            CreateTable(
                "dbo.SocietePersonnes",
                c => new
                    {
                        Societe_Id = c.Int(nullable: false),
                        Personne_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Societe_Id, t.Personne_Id })
                .ForeignKey("dbo.Societe", t => t.Societe_Id, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.Personne_Id, cascadeDelete: true)
                .Index(t => t.Societe_Id)
                .Index(t => t.Personne_Id);
            
            DropColumn("dbo.Societe", "Personne_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Societe", "Personne_Id", c => c.Int());
            DropForeignKey("dbo.SocietePersonnes", "Personne_Id", "dbo.User");
            DropForeignKey("dbo.SocietePersonnes", "Societe_Id", "dbo.Societe");
            DropIndex("dbo.SocietePersonnes", new[] { "Personne_Id" });
            DropIndex("dbo.SocietePersonnes", new[] { "Societe_Id" });
            DropTable("dbo.SocietePersonnes");
            CreateIndex("dbo.Societe", "Personne_Id");
            AddForeignKey("dbo.Societe", "Personne_Id", "dbo.User", "Identifiant");
        }
    }
}
